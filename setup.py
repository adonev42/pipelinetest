import setuptools  # type: ignore

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="detection-detection-classification",
    version="0.0.1",
    author="Team 07C",
    description="Lane Markings project",
    license="BSD",
    long_description=long_description,

    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.9",
)
