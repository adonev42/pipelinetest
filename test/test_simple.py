from unittest import TestCase
from src.backend.api.main import hello_world


class TryTesting(TestCase):
    def test_always_passes(self) -> None:
        hello_world("Test api")
        self.assertTrue(True)
